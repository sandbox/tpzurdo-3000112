(function($, Drupal) {

  var initialized;

  function init() {
    if (!initialized) {
      initialized = true;
      $('.no-result').hide();

      var settings = drupalSettings.filter_agenda.listInstance

      var options = {
        valueNames: settings.fields,
        page: settings.page,
        pagination: settings.pagination
      };

      var agendaList = new List(settings.list_id, options);

      updateList();

      function resetList(){

        agendaList.search();
        agendaList.filter();
        agendaList.update();

        $('.filter_agenda select.tag').val('any');
        $('.filter_agenda select.date').val('any');

        $('.search').val('');
      };

      function updateList(){

        var values_date = $('.filter_agenda select.date').val();
        var values_tag = $('.filter_agenda select.tag').val();

        agendaList.filter(function (item) {

          var dateFilter = false;
          var tagFilter = false;

          if(values_date == "any"){ 
            dateFilter = true;
          } else {
            dateFilter = item.values().date.includes(values_date);
          }

          if(values_tag == "any"){ 
            tagFilter = true;
          } else {
            tagFilter = item.values().category.includes(values_tag);
          }

          return dateFilter && tagFilter

        })

        agendaList.update();
      }

      $(function(){
        $('.filter_agenda select.tag').change(updateList);
        $('.filter_agenda select.date').change(updateList);

        $('.filter_agenda button.clear').click(resetList);
        
        agendaList.on('updated', function (list) {
          if (list.matchingItems.length > 0) {
            $('.no-result').hide()
          } else {
            $('.no-result').show()
          }
        });
      });

    }
  }

  Drupal.behaviors.someKey = {
    attach: function() {
      init();
    }
  };

}(jQuery, Drupal));