<?php

namespace Drupal\filter_agenda\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Datetime\DateFormatter;

/**
 * Provides a 'Filter Agenda' Block.
 *
 * @Block(
 *   id = "filter_agenda_block",
 *   admin_label = @Translation("Filter Agenda block"),
 *   category = @Translation("Filtered content"),
 * )
 */
class FilterAgendaBlock extends BlockBase implements BlockPluginInterface
{
	/**
	* {@inheritdoc}
	*/
    public function build()
    {

    	$config = $this->getConfiguration();

  		return [
  	      '#theme' => $config['template'],
  	      '#filters' => $config['filters'],
  	      '#list_id' => $config['list_id'],
  	      '#list' => $this->getList(),
  	      '#attached' => [
  	      	'drupalSettings' => [
  	      		'filter_agenda' => [
  	      			'listInstance' => $config
  	      		]
  	      	],
  	        'library' => [
  	        	'filter_agenda/list.listjs',
  	            'filter_agenda/list.instance',
  	        ],
  	      ],
  	    ];

    }

    private function filterDateFormat($date, &$date_filter)
    {
      $formatted = format_date(strtotime($date), 'custom', 'F Y');
      return $date_filter[$formatted] = $formatted;
    }

    private function summaryReduction($text, $amount)
    {
      mb_strlen($text) < $amount ? $text : mb_substr($text, 0, $amount) . '...';
    }

    public function getList()
    {

      $query = \Drupal::entityQuery('node')
        ->condition('status', NODE_PUBLISHED)
        ->condition('type', 'agenda_item')
        ->sort('field_filter_agenda_date.value' , 'ASC')
        ->range(0,200);
      $nodes = entity_load_multiple('node', $query->execute());

      $date_filter = [];

  		foreach ( $nodes as $node ) {

  			$item = new \stdClass();
     		$item->title = $node->title->value;
     		$item->body = $this->summaryReduction($node->body->summary, 50);
     		$item->start = $node->get('field_filter_agenda_date')->getValue()[0]['value'];
        $item->end = $node->get('field_filter_agenda_date')->getValue()[0]['end_value'];
        $this->filterDateFormat($item->start, $date_filter);

        $item->category = NULL;
        foreach ($node->get('field_agenda_category')->getValue() as $term_id){
          $term = Term::load($term_id['target_id']);
          \Drupal::logger('term')->debug('<pre>'.print_r($term->getName(), TRUE).'</pre>');
          $term_name = $term->getName();
          $item->categories[] = $term_name;
          $category_filter[$term_name] = $term_name;
        }

  			$list[] = $item;
  		
  		}

  		return [
        'list' => $list,
        'category_filter' => $category_filter,
        'date_filter' => $date_filter
      ];
    }

	/**
	* {@inheritdoc}
	*/
	public function defaultConfiguration()
	{
	    $default_config = \Drupal::config('filter_agenda.settings');

      \Drupal::logger('filter settings')->debug('<pre>'.print_r($default_config->get('defaultList'), TRUE).'</pre>');

	    return [
    		'list_id' => 'filter_agenda_' . rand(1, 999) . microtime(),
    		'fields' => [
    			'title',
    			'body',
    			'date',
          'category'
    		],
    		'dataFields' => [],
    		'page' => 1,
    		'pagination' => true,
    		'filters' => [
    			[ 'type' => 'search' ],
    		]
    	];
	}

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = parent::blockForm($form, $form_state);

        $config = $this->getConfiguration();

        $form['page'] = [
            '#type' => 'select',
            '#title' => $this->t('Items per page'),
            '#description' => $this->t('How many items showing in each page'),
            '#options' => range(1,50),
            '#default_value' => isset($config['page']) ? $config['page'] : '',
        ];

        $form['template'] = [
            '#type' => 'select',
            '#title' => $this->t('Template'),
            '#description' => $this->t('Select a template for the list to be shown'),
            '#default_value' => isset($config['template']) ? $config['template'] : NULL,
            '#options' => [
              'simple' => 'Simple',
              'bs3' => 'Boostrap 3',
            ],
        ];

        $form['filters'] = [
            '#type' => 'select',
            '#multiple' => TRUE,
            '#title' => $this->t('Filters'),
            '#description' => $this->t('Select which filters to apply'),
            '#options' => [
              'search' => $this->t('Search'),
              'date' => $this->t('Date'),
              'tag' => $this->t('Tag'),
            ],
            '#default_value' => isset($config['filters']) ? $config['filters'] : NULL,
        ];

        $form['paginator'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Show paginator'),
            '#description' => $this->t('If marked a paginator will appear at the bottom of the list'),
            '#default_value' => isset($config['paginator']) ? $config['paginator'] : NULL,
        ];


        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        parent::blockSubmit($form, $form_state);
        $values = $form_state->getValues();
        $this->configuration['page'] = $values['page'];
        $this->configuration['template'] = $values['template'];
        $this->configuration['filters'] = $values['filters'];
        $this->configuration['paginator'] = $values['paginator'];
        \Drupal::logger('blockSubmit')->debug('<pre>'.print_r($values, TRUE).'</pre>');
    }

    public function setConfiguration(array $configuration)
    {
        $this->configuration = $this->replaceMergeDeepArray(
          [$this->baseConfigurationDefaults(),
          $this->defaultConfiguration(),
          $configuration], FALSE
        );
    }

    public function replaceMergeDeepArray(array $arrays, $preserve_integer_keys = FALSE) {
    $result = [];
    foreach ($arrays as $array) {
      foreach ($array as $key => $value) {
        // Renumber integer keys as array_merge_recursive() does unless
        // $preserve_integer_keys is set to TRUE. Note that PHP automatically
        // converts array keys that are integer strings (e.g., '1') to integers.
        if (is_int($key) && !$preserve_integer_keys) {
            \Drupal::logger('1st')->debug(print_r($value, TRUE));
          $result[] = $value;
        }
        // I've had to replace this line from original function merge deep array cause it stacks arrays y need to keep
        elseif (isset($result[$key]) && is_array($result[$key]) && is_array($value)) {
          $result[$key] = $value;
        }
        // Otherwise, use the latter value, overriding any previous value.
        else {
          $result[$key] = $value;
        }
      }
    }
    return $result;
  }
}
